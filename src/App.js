import React, { Component } from "react";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.roomRef = React.createRef();
    this.iframeRef = React.createRef();
    this.search = this.search.bind(this);
  }

  search() {
    let queryUrl =
      "https://age.hes-so.ch/imoniteur_AGEP/!GEDPUBLICREPORTS.html?ww_i_reportModel=4140578784&ww_i_reportModelXsl=4140578861&ww_x_UNITEORG=557&ww_x_ESPACE=";
    let room = this.roomRef.current.value;
    let url = queryUrl + room;
    // document.getElementById("room").innerHTML = url;
    // document.getElementById("ifr").src = url;
    this.iframeRef.current.src = url;
  }

  render() {
    return (
      <div className='App'>
        <header className='App-header'>
          <h1>Roomr</h1>
          <div>
            Enter a room number:
            <input ref={this.roomRef} type='text' placeholder='B5.01' />
            <input
              className='button'
              type='button'
              value='Search'
              onClick={this.search}
            />
          </div>
          <br />
          <iframe ref={this.iframeRef} id='ifr' scrolling='no' title='avails' />
        </header>
      </div>
    );
  }
}

export default App;
